//This is the context
public class Player {

    private String pseudo;
    private State state;

    public Player(String pseudo){
        this.pseudo = pseudo;
        this.state = new HealthyState();
    }

    // getter
    public String getPseudo(){
        return this.pseudo;
    }

    // setter
    public void setState(State newState){
        this.state = newState;
    }

    //actions
    public void regenerateHealth(){
        state.regenerateHealth(this);
    }
    public void suicide(){
        state.suicide(this);
    }
    public void resurrect(){
        state.resurrect(this);
    }
    public void useSuperPower(){
        state.useSuperPower(this);
    }
}
