public class HurtState implements State {

    public String toString() {
        return "Hurt State";
    }

    @Override
    public void regenerateHealth(Player p) {
        System.out.println(p.getPseudo() + ": your health is now full.");
        p.setState(new HealthyState());
    }

    @Override
    public void suicide(Player p) {
        System.out.println(p.getPseudo() + ": you just killed yourself. RIP");
        p.setState(new DeadState());
        System.out.println(p.getPseudo() + ": you're now in Dead State.");

    }

    @Override
    public void resurrect(Player p) {
        System.out.println(p.getPseudo() + ": you can't ressurect when you're alive.");
    }

    @Override
    public void useSuperPower(Player p) {
        System.out.println(p.getPseudo() + ": you're hurt, you can't use your superpower");
    }
}