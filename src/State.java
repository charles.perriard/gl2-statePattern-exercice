public interface State {
    public void regenerateHealth(Player p);
    public void suicide(Player p);
    public void resurrect(Player p);
    public void useSuperPower(Player p);
    //TODO add an action
}