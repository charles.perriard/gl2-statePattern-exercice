import java.util.Random;

public class Demo {
    public static void main(String[] args) {

        Player p = new Player("gaga46");
        Random rdm = new Random();


        for (int i = 0; i < 15; i++) {
            int rdmAction = rdm.nextInt(4);
            switch (rdmAction){
                case 0:
                    System.out.println("call regenerateHealth()");
                    p.regenerateHealth();
                    break;
                case 1:
                    System.out.println("call useSuperPower()");
                    p.useSuperPower();
                    break;
                case 2:
                    System.out.println("call resurrect()");
                    p.resurrect();
                    break;
                case 3:
                    System.out.println("call suicide()");
                    p.suicide();
                    break;
            }
            System.out.println("-----------------------------------------------------------------------");
        }

    }
}