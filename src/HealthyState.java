public class HealthyState implements State {

    @Override
    public void regenerateHealth(Player p) {
        System.out.println(p.getPseudo() + ": your health is already full.");
    }

    @Override
    public void suicide(Player p) {
        System.out.println(p.getPseudo() + ": you just killed yourself. RIP");
        p.setState(new DeadState());
        System.out.println(p.getPseudo() + ": you're now in Dead State");
    }

    @Override
    public void resurrect(Player p) {
        System.out.println(p.getPseudo() + ": you can't resurrect when you're alive.");
    }

    @Override
    public void useSuperPower(Player p) {
        System.out.println(p.getPseudo() + ": you killed everyone with your amazing power but you hurt yourself");
        p.setState(new HurtState());
        System.out.println(p.getPseudo() + ": you're now in Hurt State");
    }
}