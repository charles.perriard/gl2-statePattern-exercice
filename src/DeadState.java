public class DeadState implements State {

    @Override
    public void regenerateHealth(Player p) {
        System.out.println(p.getPseudo() + ": you're dead, you can't regenerate your health.");
    }

    @Override
    public void suicide(Player p) {
        System.out.println(p.getPseudo() + ": you're already dead, you can't commit suicide.");
    }

    @Override
    public void resurrect(Player p) {
        System.out.println(p.getPseudo() + ": you resurrected yourself.");
        p.setState(new HurtState());
        System.out.println(p.getPseudo() + ": you're now in Hurt State.");
    }

    @Override
    public void useSuperPower(Player p) {
        System.out.println(p.getPseudo() + ": you're dead, you can't use your superpower");
    }
}